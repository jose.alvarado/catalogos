import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DesarrolloDataSource} from '../datasources';
import {Persona, PersonaRelations} from '../models';

export class PersonaRepository extends DefaultCrudRepository<
  Persona,
  typeof Persona.prototype.folio,
  PersonaRelations
> {
  constructor(
    @inject('datasources.desarrollo') dataSource: DesarrolloDataSource,
  ) {
    super(Persona, dataSource);
  }
}
