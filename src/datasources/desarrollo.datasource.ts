import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'desarrollo',
  connector: 'loopback-connector-mongodb',
  url: 'mongodb://admin:WSadmin90sistemas@cluster0-shard-00-00.naus5.mongodb.net:27017,cluster0-shard-00-01.naus5.mongodb.net:27017,cluster0-shard-00-02.naus5.mongodb.net:27017/test?ssl=true&replicaSet=atlas-1pz0qm-shard-0&authSource=admin&retryWrites=true&w=majority',
  port: 27017,
  user: 'admin',
  password: 'WSadmin90sistemas',
  database: 'test',
  useUnifiedTopology: true,
  useNewUrlParser: false
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class DesarrolloDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'desarrollo';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.desarrollo', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
